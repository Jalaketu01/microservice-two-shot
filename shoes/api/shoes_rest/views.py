from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Shoe, BinVO  # May need to edit later
import json

# quick update the view on shoe
class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "import_href"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model_name",
        "manufacturer",
        "color",
    ]  # remove the bin since make the extradata already

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name, "picture": o.picture, "id": o.id}
        # using {"shoe": o.shoe.name} in the beginning
        # might be 'bin':o.bin.closet_name since it's should be referenced value


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["model_name", "manufacturer", "color", "picture", "id", "bin"]

    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse({"shoes": shoes}, encoder=ShoeListEncoder, safe=False)
        # This one should be fine without safe=False ?
    else:
        content = json.loads(request.body)
        print(content, content["bin"])
        try:
            print(content)
            bin = BinVO.objects.get(id=content["bin"])
            print(bin)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Bin"},
                status=400,
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


# @require_http_methods(["GET", "POST"])
# def api_list_shoes(request, bin_vo_id=None):
#     if request.method == "GET":
#         # edited to gather the list based on the bin_vo_id. If not available, get all
#         if bin_vo_id is not None:
#             shoes = Shoe.objects.filter(bin=bin_vo_id)
#         else:
#             shoes = Shoe.objects.all()
#         return JsonResponse(
#             {"shoes": shoes},
#             encoder=ShoeListEncoder,
#         )
#     else:  # Handle POST request
#         content = json.loads(request.body)

#         # Get the object and put it in the content dict
#         try:
#             shoe_href = content["bin"]
#             print(content)
#             print(shoe_href)
#             bin = BinVO.objects.get(import_href=shoe_href)
#             content["bin"] = bin
#         except BinVO.DoesNotExist:
#             return JsonResponse(
#                 {"message": "Invalid bin id"},
#                 status=400,
#             )

#         shoe = Shoe.objects.create(**content)
#         return JsonResponse(
#             shoe,
#             encoder=ShoeDetailEncoder,
#             safe=False,
#         )


# # This function is not working correct
@require_http_methods(["GET", "PUT", "DELETE"])
def api_detail_shoes(request, pk):
    if request.method == "GET":
        try:
            shoes = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoes,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Shoe does not exist"})
            response.status_code = 404
            return response

    elif request.method == "PUT":  # Still include PUT for now.
        content = json.loads(request.body)
        Shoe.objects.filter(id=pk).update(**content)
        shoes = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"delete": count > 0})
