from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    closet_name = models.CharField(max_length=200)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.closet_name

    class Meta:
        ordering = (
            "closet_name",
            "bin_number",
            "bin_size",
        )

    # 11-19 might be optional
    # Default ordering for Bin from wardrobe
    # This order matters. BinVO need to be made so the Shoe can use it.


class Shoe(models.Model):
    model_name = models.CharField(max_length=200)
    manufacturer = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture = models.URLField()
    bin = models.ForeignKey(
        BinVO,
        on_delete=models.CASCADE,
        related_name="shoes",
    )

    def __str__(self):
        return self.model_name

    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})

    # 36-41 might be optional
