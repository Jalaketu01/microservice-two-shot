from django.urls import path
from .views import api_list_shoes, api_detail_shoes

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:pk>/", api_detail_shoes, name="api_detail_shoes"),
    # path("bins/", api_list_bins, name="api_list_bins"),
    # path("bins/<int:pk>/", api_detail_shoes, name="api_bins_shoes"),
    # url or view still not working. delete instead get.
    # 2022-12-03 12:56:19 Method Not Allowed (POST): /api/shoes/1/
    # 2022-12-03 12:56:19 [03/Dec/2022 20:56:19] "POST /api/shoes/1/ HTTP/1.1" 405 0
]
