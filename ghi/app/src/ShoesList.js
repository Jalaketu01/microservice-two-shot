import React from 'react';
import loadShoes from '.';

function ShoesList(props) {
    const deleteShoes = async (id) => {

        const deleteFunctionUrl = `http://localhost:8080/api/shoes/${id}`
        const options = {
            method: 'DELETE'
        }
        const response = await fetch(deleteFunctionUrl, options)
        if (response.ok) {
            console.log(`The slelected shoe has been deleted with an id of ${id}`)
            // Updating the state in the parent component to trigger an update in the view
            loadShoes()
        }
    }

    if (props.shoes === undefined) {
        return (<div className="px-4 py-5 my-5 text-center">Nothing here yet. Add new shoes</div>
        )
    }
    return (
        <table className="table table-dark table-striped">
            <thead>
                <tr>
                    <th>Model Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                    <th>Color</th>
                    <th>Bin</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes.map((shoe) => {
                    return (
                        <tr key={shoe.id}>
                            <td>{shoe.model_name}</td>
                            <td>{shoe.manufacturer}</td>
                            <td><img src={shoe.picture} alt={shoe.model_name} width="200px" height="200px" /></td>
                            {/* img elements must have an alt prop,
                            either with meaningful text, or an empty string for decorative images
                            jsx-a11y/alt-text */}
                            <td>{shoe.color}</td>
                            <td>{shoe.bin}</td>
                            <td><button
                                onClick={() => deleteShoes(shoe.id)}
                            >Delete This Shoes</button></td>
                        </tr>


                    )
                })}

            </tbody>
        </table>
    );
}



export default ShoesList
