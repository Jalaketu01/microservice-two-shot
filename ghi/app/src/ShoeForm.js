import React from 'react';

class ShoeForm extends React.Component {
    //build constructor based on what input we expect
    constructor(props) {
        super(props)
        this.state = {
            modelName: "",
            manufacturer: "",
            color: "",
            picture: "",
            bin: "",
            //don't forget need one for the entry, one for handling all data
            bins: [],
        };

        this.handleModelNameChange = this.handleModelNameChange.bind(this);
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handlePictureChange = this.handlePictureChange.bind(this);
        this.handleBinChange = this.handleBinChange.bind(this);
        // Need to bind handle submit
        this.handleSubmit = this.handleSubmit.bind(this)

    }
    // get from from html, and update input with closing "/".
    // Replace class to className
    // Replace for to htmlFor
    // Replace selected to value
    //   Before:
    //   <select required name="conference" id="conference" className="form-select">
    //   <option selected value="">Choose a conference</option>
    //   </select>
    //   After:
    //     <select onChange={this.handleConferenceChange} value={this.state.conference} name="conference" id="conference" className="form-select">
    //     <option value="">Choose a conference</option>
    //

    handleModelNameChange(event) {
        const value = event.target.value;
        this.setState({ modelName: value })
    }

    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({ manufacturer: value })
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value })
    }

    handlePictureChange(event) {
        const value = event.target.value;
        this.setState({ picture: value });
    }

    handleBinChange(event) {
        const value = event.target.value;
        this.setState({ bin: value });
    }
    // The first thing  to check is the bin drop down, so it should fatch the bins first
    async componentDidMount() {
        const binUrl = 'http://localhost:8100/api/bins/';
        // wadrobe
        const response = await fetch(binUrl);

        if (response.ok) {
            const data = await response.json();
            this.setState({ bins: data.bins });
        }
        // We are getting multiple bins from the serverr so it should be pl
    }
    //summit function

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.model_name = data.modelName;
        console.log(data)
        console.log(this)

        delete data.modelName;
        // delete extra data that serever don't take. especially delete data.conferences

        delete data.bins;
        console.log('after delete')
        console.log(data)

        const shoeUrl = `http://localhost:8080/api/shoes/`;


        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);

            this.setState({
                modelName: "",
                manufacturer: "",
                color: "",
                picture: "",
                bin: "",
            });
        }
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new shoe</h1>
                        {/* function onSubmit */}
                        <form onSubmit={this.handleSubmit} id="create-presentation-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleModelNameChange} value={this.state.modelName} placeholder="Model name" required type="text" name="model_name"
                                    id="model_name" className="form-control" />
                                <label htmlFor="model_name">Model name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleManufacturerChange} value={this.state.manufacturer} placeholder="Manufacturer" type="text" name="manufacturer" id="manufacturer"
                                    className="form-control" />
                                <label htmlFor="manufacturer">Manufacturer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="color"
                                    id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePictureChange} value={this.state.picture} placeholder="Picture" required type="text" name="picture" id="picture"
                                    className="form-control" />
                                <label htmlFor="picture">Picture</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleBinChange} value={this.state.bin} name="bin" id="bin" className="form-select">
                                    <option value="">Choose a bin</option>
                                    {this.state.bins.map(bin => {
                                        return (
                                            //id is the key for presentation to get intpo the conference, so we use it as a key.
                                            <option key={bin.id} value={bin.id}>
                                                {bin.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default ShoeForm
