import React from 'react';

function HatsList(props) {
    if (props.hats === undefined) {
        return (<div className="px-4 py-5 my-5 text-center">Nothing here yet. Add new hats</div>
        )
    }
    return (
        <table className="table table-dark table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Fabric</th>
                    <th>Style</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map((hat) => {
                    return (
                        <tr key={hat.id}>
                            <td>{hat.name}</td>
                            <td>{hat.fabric}</td>
                            <td>{hat.style}</td>
                            <td>{hat.color}</td>
                            <td><img src={hat.picture} alt={hat.name} width="200px" height="200px" /></td>
                            <td>{hat.location}</td>
                        </tr>


                    )
                })}

            </tbody>
        </table>
    );
}
export default HatsList
