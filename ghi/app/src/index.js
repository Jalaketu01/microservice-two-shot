import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>
// );

const root = ReactDOM.createRoot(document.getElementById('root'));
async function loadShoes() {
  const response = await fetch('http://localhost:8080/api/shoes/');
  const responseHat = await fetch('http://localhost:8090/api/hats/');
  if (response.ok && responseHat.ok) {
    const data = await response.json();
    const dataHat = await responseHat.json();
    root.render(
      <React.StrictMode>
        <App shoes={data.shoes}
        hats={dataHat.hats} 
        />
      </React.StrictMode>
    );
  } else {
    console.error(response);
  }
}
loadShoes();
export default loadShoes

// async function loadHats() {
//   if (responseHat.ok) {
//     // const dataHat = await responseHat.json();
//     root.render(
//       <React.StrictMode>
//         {/* <App hats={dataHat.hats} /> */}
//       </React.StrictMode>
//     );
//   } else {
//     console.error(response);
//   }
// }
// loadHats();
