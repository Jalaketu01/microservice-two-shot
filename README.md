# Wardrobify

WARDROBIFY! provides a service of keeping track of your shoes and hats! <br />
Please Fork it through: https://gitlab.com/Jalaketu01/microservice-two-shot <br />

Team:

* Brian - Hats
* Chengyun - Shoes


## Design

See Excalidraw: https://excalidraw.com/#room=4e0a5dfc1a30c2015acf,tYz0afNLn9gx2ExJ3oBQKg

# General info of service
The services are built in docker.
- React service binds to port "3000:3000"<br />
Open [http://localhost:3000] to view the react.<br />
- Wardrobe API service binds to port "8100:8000"<br />
Open [http://localhost:8100] to access to locations and bins.<br />
- Hats API binds to port "8090:8000"<br />
Open [http://localhost:8090] to access to hats.<br />
- Shoes API binds to port "8080:8000"<br />
Open [http://localhost:80800] to access to shoes.<br />
- Database is built base on postgres.<br />

## Shoes microservice

**Back-end**<br />
Build a bin model to handle the information of bins from the wardrobe-api.<br />
Create a shoe model that handle the data of shoes that we are going to make.<br />

To create a new item (shoes) need five major inputs:
- model_name        Input type: string
- manufacturer      Input type: string
- color             Input type: string
- picture           Input type: url
- bin               Input type: positive interger

**Note**: bin option will be a drop-down trhough the front-end (i.e. react)<br />
**Note**: To successfully create a item(shoes), you will need to generate a bin first.<br />
- Open [http://localhost:8100/api/bins/] to view the existed bins.

To create a new bin need three major inputs:
- closet_name      Input type: string
- bin_number       Input type: positive integer
- bin_size         Input type: positive integer

The poller will collect the info from the wardrobe-api and make the corresponding value object.

**Front-end**<br />
Using react to handle the components.
- Open [http://localhost:3000] to view the react.

For shoes, there are one list component and one form component.
- The list componenet to handle the info of shoes (get request).
- The form componenet is made with corresponding metics mentioned above.
Users are able to create a new item(shoes) though submitting a post request.

## Hats microservice

BACK END

models.py:
    - Defines the LocationVO and Hat models.
poller.py:
    - Imports LocationVO from models.
    - get_location function polls for Location data at [http://wardrobe-api:8000/api/locations/] and uses it to create LocationVO instances.
views.py:
    - LocationVODetailEncoder, HatListEncoder, and HatDetailEncoder convert data to correct shape for JSON.
    - api_list_hats function shows either a list of all Hats (if request.method == "GET") or creates a new Hat (else).
    - api_hat_detail function deletes a Hat based on pk.
urls.py:
    - Contains URL patterns for api_list_hats ("hats/") and api_hat_detail ("hats/<int:pk>/").

FRONT END

index.js:
    - Defines the Root element used to render the React DOM.
HatsList.js:
    - Defines the function component HatsList.
    - HatsList sends a GET request to "hats/" to populate a list of all hats. A button in HatsList will onClick send a DELETE request and a corresponding #pk to "hats/<int:pk>/" to delete that Hat.
 HatForm.js:
     - Defines the class component HatForm.
     - HatForm allows users to fill in Hat fields. It uses an onChange function in the form and a this.handle function in the component to record the state of the input fields if changes are made.
     - When all the fields are filled in by the user, they can press a button to send the form data and a POST request to "hats/" to create a new Hat.
