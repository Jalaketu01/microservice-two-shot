from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(
        max_length=100,
        null=True,
    )


class Hat(models.Model):
    name = models.CharField(max_length=200)
    fabric = models.CharField(max_length=200)
    style = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture = models.URLField()
    location = models.ForeignKey(
        LocationVO,
        on_delete=models.CASCADE,
        related_name="hats",
    )
    
